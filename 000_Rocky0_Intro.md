# Rocky Linux Series #0:  Intro, and How Rocky Linux Came to Be

*Date: 2021-09-08*

This is the beginning of a "behind the scenes" look at Rocky Linux and how it's built.  In a series of articles, I'm going to try and give a kind of guided tour about the pieces that we use to build the distro, and why things are set up the way that they are.

My hope is to make this series technical, but still accessible to the average Linux enthusiast.  There's a lot to cover, and it can be intimidating if you try and take it all in at once!  My aim is to give a good overview, and answer the hows and whys of the Rocky build infrastructure.  In-depth specifics can better be answered in the documentation sites or manpages of the individual software pieces.  If you want API references or comprehensive command line options, you're not in the right place!  (but you will be linked to the right place, so read anyways ;-)  )

<br />

## The basics: What is Rocky Linux?

**Rocky Linux ( [https://rockylinux.org](https://rockylinux.org/) )** is a Linux distribution designed to be a 100% compatible recompile/rebuild of the popular Red Hat Enterprise Linux(tm) distribution.  The project was formed in December 2020, after it was announced that CentOS Linux was stopping development and focusing on the newer "CentOS Stream" distro.  Rocky Linux aims to pick up where CentOS left off, as a 100% compatible recompile of Red Hat Enterprise 8.

If you are not familiar with Red Hat Enterprise Linux (RHEL), there are some things you ought to know about it:


- Major releases happen once every 3 years, based on a "frozen in time" version of Fedora Linux, which acts as the upstream source.  (RHEL 8 is based on Fedora 28)
- Releases are supported with updates for **10(!)** years.  The current major version of Rocky/RHEL is 8, and both will receive package updates until **May 2029**.
- The package version choices are *extremely* stable and conservative, perhaps more so than any other Linux distro out there.  Most packages are pinned at a major version, and you generally won't receive large changes in upgrades.


That last point bears repeating, as it has come up a lot from those who aren't familiar with the RHEL ecosystem.  The name of the game is stability, predictability, and support-ability.  If you want the absolute latest packages and bleeding edge of Linux/Open Source development, this is probably not your cup of tea.  And that's okay!  People need different things.  Rocky is based on RHEL, and the virtues of each are the same: stable, well-tested, and guaranteed compatibility.

That's not to say we're all stuffy and boring!  There are exciting pieces you can add on to Rocky that extend the hardware it runs on, or makes newer pieces of some software available, etc etc.  But the core Rocky Linux distribution will always have the same mission:  match Red Hat Enterprise package for package, "bug for bug", as closely as we are able!


<br />

## How did Rocky Linux come to be?

On December 8, 2020, an announcement was made on the CentOS blog:  [https://blog.centos.org/2020/12/future-is-centos-stream/](https://blog.centos.org/2020/12/future-is-centos-stream/).  You can read it for yourself, but the 2 takeaways most people got were:  
  
- CentOS proper is canceled in favor of CentOS Stream, which is a kind of rolling preview of RHEL (supported until 2024)
- CentOS 8 updates will end in **December 2021**, as opposed to the originally planned **May 2029**

You can tell from the comments, reactions to this announcement were mixed, at best.  Those blog comments contain the origin of Rocky Linux:

![Rocky Linux, but we didn't know it yet](blog_image/rocky_origin.png)

Gregory Kurtzer, who was a co-founder of CentOS from "back in the day", saw that there might be some demand for a replacement to CentOS 8.  He pointed everyone to a Slack chat space he had from another project, and approximately 5,000 people showed up over the next 10 hours or so.  Absolute beautiful chaos ensued!  Chat messages scrolled past faster than could be read.  It was clear from the pure energy and passion from all the people interested in this that something special was on its way.

Volunteers were organized into chat channels, to control the chaos of messages if nothing else.  A development channel was one of the first formed, and the Rocky Release Engineering team was formed, though we didn't realize what we were at the time.  We immediately began ~~vigoro~~ friendly technical discussions to determine how exactly we were going to pull this thing off.  The other articles in this series will hopefully explain the decisions, reasoning, and revelations that were made along the way.

Addendum:  Stop asking us to change the name!  It won't be changed!  The story is now famous, it was suggested by Greg in the first day of the project's existence in the chaotic chat channel.  Support for it was nearly unanimous when it was suggested.  We should honor those who have come before us, see the Rocky Linux FAQ for more info :-)

<br />

## So who the heck are you?

Hi there, nice to meet you!  My name is Skip Grube, and I am on the development/release engineering team for Rocky Linux.  I help with package debugging, compilation, the Raspberry Pi port, (some) technical documentation, scripts, and just generally wherever I'm needed.  I consider myself the co-meme chairman of our release engineering team, a distinctly important position!

<br />

## ...And why are you writing this?
I'm writing this in part to de-mystify some of the technology around the Fedora and RHEL/CentOS build systems.  There are many moving pieces, and each is documented *mostly* well enough.  But there isn't a "big picture" document (that I've seen) which puts these concepts into perspective and explains how they all work together.  I want to take the ~~pain and suff~~ fun times that our dev team has gone through over the course of about 6 months.  I want to record what we've learned, and help all who come after us!

<br />

## Let's get started!  Next time...

My next article will be a **Build Overview**.  We'll take a brief look at all the moving pieces of software that are used to build Rocky Linux, what exactly they do, and where you can find more info about them.


<br />




**Contact**

Thanks for reading!  If you have any questions about the material, or I guess questions in general, feel free to ping me:

- Chat:  @skip77 on https://chat.rockylinux.org
- Email: skip (at) rockylinux.org

<br />




