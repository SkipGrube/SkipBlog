<h1 id="rocky-linux-series-4-build-dependencies-or-it-s-complicated-">Rocky Linux Series #4:  Build Dependencies (or: &quot;It&#39;s Complicated&quot;)</h1>
<p><img src="blog_image/rpm_strangle.png" alt="Dependency debugging, illustrated:" title="Dependency debugging, illustrated:"></p>
<p><em>Date: 2021-10-15</em></p>
<p>Today we&#39;ll go over RPM dependencies, and why they may not be as simple as they seem.</p>
<p><br /></p>
<h2 id="rpm-specs-quick-review">RPM Specs: Quick Review</h2>
<p>RPMs have <code>Requires:</code> and <code>Provides:</code> listings which indicate the dependencies they need, and dependencies that they provide.  These are part of the <code>.spec</code> file, the thing that defines the RPM package and how to build it.</p>
<p>Every DNF/yum repository maintains a collective gigantic index of these Requires and Provides, that cover all packages contained within.  The indexes are searchable, so when DNF comes across a <code>Requires</code> that is not currently installed on  your system, it can check the index and find which package(s) <code>Provide</code> it.</p>
<p><b>Quick Example:</b></p>
<p>Let&#39;s look at the DNF package manager itself - what does that package require?  We can find out with <code>repoquery</code> , like this:</p>
<pre><code>skip@skip-rpi:~$ repoquery <span class="hljs-comment">--requires dnf</span>
Last metadata expiration <span class="hljs-keyword">check</span>: <span class="hljs-number">0</span>:<span class="hljs-number">00</span>:<span class="hljs-number">26</span> ago <span class="hljs-keyword">on</span> Thu <span class="hljs-number">14</span> <span class="hljs-keyword">Oct</span> <span class="hljs-number">2021</span> <span class="hljs-number">08</span>:<span class="hljs-number">56</span>:<span class="hljs-number">13</span> PM EDT.
/<span class="hljs-keyword">bin</span>/sh
python3-dnf = <span class="hljs-number">4.4</span><span class="hljs-number">.2</span><span class="hljs-number">-11.</span>el8
</code></pre><p>So one of the requires is <code>/bin/sh</code>, a basic shell.  Let&#39;s see what package <code>Provides</code> that:</p>
<pre><code>skip<span class="hljs-meta">@skip</span>-<span class="hljs-string">rpi:</span>~$ dnf whatprovides <span class="hljs-string">'/bin/sh'</span>
Last metadata expiration <span class="hljs-string">check:</span> <span class="hljs-number">0</span>:<span class="hljs-number">02</span>:<span class="hljs-number">53</span> ago on Thu <span class="hljs-number">14</span> Oct <span class="hljs-number">2021</span> <span class="hljs-number">08</span>:<span class="hljs-number">56</span>:<span class="hljs-number">13</span> PM EDT.
bash<span class="hljs-number">-4.4</span><span class="hljs-number">.20</span><span class="hljs-number">-1.</span>el8_4.<span class="hljs-string">aarch64 :</span> The GNU Bourne Again shell
<span class="hljs-string">Repo        :</span> baseos
Matched <span class="hljs-string">from:</span>
<span class="hljs-string">Provide    :</span> <span class="hljs-regexp">/bin/</span>sh
</code></pre><p>So we see that the <code>bash</code> package from Rocky&#39;s <code>BaseOS</code> repository provides the /bin/sh shell that DNF requires.  Pretty simple takeaway: RPMs have things they require and provide, and DNF (or the older Yum) is very good at tracking down and matching these.</p>
<p><br /><br /></p>
<h2 id="build-time-requirements">Build Time Requirements</h2>
<p>In addition to <code>Requires</code> entries, source RPM .spec files have another kind of entry, called <code>BuildRequires:</code>.  These are the dependencies that are needed to <em>build</em> the RPM, but not to install or run it.  This makes them distinct from the <code>Requires</code> entries, but they often overlap.</p>
<p>A simple example of this:  Many packages contain software written in C, and require the <strong>gcc</strong> package to compile their source.  But obviously they don&#39;t need gcc to in order to install or run on your system!  Gcc would therefore be a <code>BuildRequires</code> part of that source RPM.</p>
<p>Gathering and keeping track of these BuildRequires is hugely important when building a distribution, as they tend to be more numerous and more complicated than the simple install-time requirements of each package.</p>
<p><br /><br /></p>
<h2 id="how-do-we-start-hint-look-to-your-bootstraps-">How do we start?  (Hint: Look to your bootstraps!)</h2>
<p>If we&#39;re going to build an entire RHEL 8&#39;s worth of RPM packages (ie. several thousand), we must have some kind of a base to start with.  Even the simplest, most &quot;basic&quot; packages have plenty of <code>BuildRequires</code>, and we must satisfy them to even begin building everything.</p>
<p>In early development, the Rocky Linux team used <strong>CentOS 8.4</strong> as a base to begin building our packages.  Once we build a full set of RPMs using the CentOS repositories as requirements, we can put the set of new packages we produced in a repository.  Any further builds can now do ourselves by pointing to the new Rocky repository instead of CentOS - now we have become self-building!</p>
<p>I&#39;ve heard different names for this process, most often called Repository or Distribution Bootstrapping.  We started with nothing, and used an outside source (in this case CentOS 8) to get us going!</p>
<p><br /><br /></p>
<h2 id="-devel-and-hidden-dependencies">*-devel and &quot;Hidden&quot; Dependencies</h2>
<p>There&#39;s something about building RHEL in particular that many people don&#39;t realize:  Not all of these build dependencies are present in the RHEL repositories!  Let me explain:
<br /></p>
<p>Packages often require <code>*-devel</code> packages from libraries at build-time to compile successfully.  These -devel packages can contain header files, or other relevant information that tell a piece of code how to build against a required library.  For example, the popular <strong>bind</strong> package (a DNS server) requires <strong>openssl-devel</strong> to compile successfully, because Bind must communicate with the openssl library for its cryptographic functions.</p>
<p>These -devel libraries are generated along with their matching main package during an RPM build.  When you build the &quot;openssl&quot; package, openssl-devel is also produced alongside it.  Unfortunately, many of these -devel packages are not available in the Red Hat repositories.  You cannot build all of the packages in RHEL 8 using onlythe RHEL 8 (or CentOS 8) repos.  You must go out and produce those missing -devel packages by compiling the appropriate RPMs yourself!
<br /></p>
<p>Complicating matters further, some packages depend on packages not even present in RHEL 8.  Going back to our <strong>bind</strong> example, one of its BuildRequires is the package <strong>kyua</strong>.  Kyua is NOT available in any RHEL  package repository.  Kyua in turn depends on the <strong>lutok</strong> package, which itself depends on <strong>atf</strong>.  This chain of dependencies needs to be built and available before we can produce <strong>bind</strong>!</p>
<p>Fortunately, all of these packages are available as Red Hat sources from <a href="https://git.centos.org">https://git.centos.org</a> .  All the sources are well-maintained, and are easy to clone via Git (see previous article for more info about code storage).  One of the early tasks in the primordial stages of the Rocky Linux build process was gathering all of these dependencies and figuring out the entire list of sources that need to be imported.  We see above that it&#39;s not as simple as just &quot;import all the packages that are in RHEL&quot;, many more than that are needed to do all the builds.  You can see some of the dev team&#39;s early work on this documented in the Rocky Linux wiki, like on <a href="https://wiki.rockylinux.org/en/team/development/Package_External_Dependencies">this page</a>.</p>
<p><br /><br /></p>
<h2 id="let-s-talk-briefly-about-modules">Let&#39;s talk briefly about: MODULES</h2>
<p>Modules are groups of related packages that get built together and often with special build-time options.</p>
<p>For example, the <strong>mariadb</strong> module is for building MariaDB 10.3, 10.5, and all their related packages (as of this writing).  The specification for building modules is written in YAML, and exists under the modules/ subdirectory in Gitlab.  Example:  the YAML to build MariaDB 10.5 is <a href="https://git.rockylinux.org/staging/modules/mariadb/-/blob/r8-stream-10.5/mariadb.yaml">located here</a>.</p>
<p>In the linked YAML, we see that there is a &quot;build order&quot; in which the packages need to be built.  Packages later in the order will have BuildRequires on packages earlier in the build order, so the earlier packages must be built first.  There is also a &quot;macros&quot; section, which defines special macros (variables) applied just to packages within this module.  Additionally, other options in this YAML file are possible: Like a module that depends upon a particular version of another module to be enabled!</p>
<p>Now that we see how they work, it&#39;s apparent that building modules by hand is quite a chore.  Each package in the module needs to be built in the correct order, and you&#39;d need a unique Mock config file for <em>each package in the module</em>, all containing custom options.  And after each individual RPM build, you would have to commit that package to a local repository, so it could be available as a <code>BuildRequires</code> for the next packages in the module&#39;s build order.</p>
<p>Fortunately, we have tools to automate this tedious work:  <a href="https://pagure.io/fm-orchestrator">MBS (Module Build Service</a> (officially), and Rocky&#39;s own Ansible-based <a href="https://github.com/nazunalika/lazybuilder">&quot;Lazy Builder&quot;</a>  (very unofficial, used for local builds).  These tools are designed to read the YAML file for the module and execute these steps automatically.  Much less tedious when our tools do this for us!</p>
<p><strong>A module comparison:</strong>
I like to think of the module YAML files as analogous to the <code>.spec</code> files of individual source RPMs.  As each spec file is a kind of &quot;recipe&quot; for building an RPM package, each YAML is a recipe for building a modular group of packages (like our MariaDB example).</p>
<p><br /><br /></p>
<h2 id="conclusion">Conclusion</h2>
<p>I hope you learned something about package (and module!) building from this article.  My goal is to introduce the basic concepts, but also illustrate some of the complexity in doing this for an entire Linux distribution.</p>
<p>In my next article, I&#39;m planning something special:  <strong>a package building lab!</strong>  We&#39;ll walk through grabbing the source for a couple of Rocky packages, and building them locally, from the ground up.  With specific, step-by-step instructions.  And commentary by me along the way, of course!  Stay tuned!</p>
<p><br /></p>
<p>Thanks for reading,</p>
<p>-Skip</p>
<p><br /></p>

