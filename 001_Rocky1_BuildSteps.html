<h1 id="rocky-linux-series-1-build-steps">Rocky Linux Series #1:  Build Steps</h1>
<p><img src="blog_image/build_logo.png" alt="The distro engineer&#39;s motto" title="The distro engineer&#39;s motto"></p>
<p><em>Date: 2021-09-11</em></p>
<p>So we want to reproduce Red Hat Enterprise Linux from the ground up.  <em>But how?</em>   Read on, and buckle up.  Today we&#39;ll cover the 10,000 foot overview of what the &quot;build steps&quot; are: Where source comes from, how it gets to Rocky Linux, and how it gets (sometimes) patched, compiled, and produced as a Rocky package.</p>
<p><br /></p>
<h2 id="quick-review-rpms">Quick Review:  RPMs</h2>
<p>Rocky Linux, like RHEL, Fedora, CentOS Stream, etc. is an <strong>RPM</strong>-based distribution.  This means that packages are stored in the RPM format and live in Yum/DNF repositories.  Importantly, it also means that there are &quot;source RPMs&quot; (SRPMs) for each package.  These SRPMs contain source code, patches, and a spec file &quot;recipe&quot; with instructions on how to compile that package.</p>
<p>Rocky Linux as a distribution is made up almost entirely of these RPMs.  By and large, the business of creating Rocky mostly revolves around importing, compiling, and publishing these packages.  There are more than <strong>5000</strong> of them in our base repositories alone, and they all must be built, maintained, tested, and released.</p>
<p>This isn&#39;t going to be an RPM tutorial, those are available elsewhere online. I&#39;m assuming you are at least a little familiar with the basic concepts of RPM packages.  For this article, the most important thing to know is the idea of creating source RPMs from source code, and then producing binary/final RPMs from those SRPMs.</p>
<p><br />
Basic RPM compilation looks like:  </p>
<p> <strong>Source Code (Git)   ----&gt;    Source RPM (SRPM)    ------&gt;  Binary RPM (final, goes in repository)</strong></p>
<p><br /></p>
<h2 id="the-steps">The Steps</h2>
<p>We&#39;ll list the broad steps here, then go into each one individually:  </p>
<ol>
<li><p><strong>Import the source code for a RHEL package into Rocky&#39;s source control</strong></p>
<ul>
<li>Git tags must be translated appropriately</li>
<li>&quot;Modular stream&quot; packages may require special care (more on this later)<br /><br /></li>
</ul>
</li>
<li><p><strong>When importing some packages, certain Rocky-specific patches need to be applied</strong></p>
<ul>
<li>Debranding needs to happen - no Red Hat(tm) logos, trademarks, etc. are allowed to be redistributed</li>
<li>Occasionally, build failures (against newer libraries) require a small fix-it patch to allow compilation<br /><br /></li>
</ul>
</li>
<li><p><strong>Package must be compiled into an SRPM, then into a binary RPM, and internal tests run</strong></p>
<ul>
<li>In most modern RPM build systems, this is effectively one step.  </li>
<li>SRPM production, SRPM -&gt; Binary compilation, and then automated tests are all run in quick succession<br /><br /></li>
</ul>
</li>
<li><p><strong>New packages then get signed</strong></p>
<ul>
<li>RPM packages are cryptographically signed with a private Rocky key</li>
<li>Ensures 100% that these packages were in fact produced by Rocky Linux, and not some imposter<br /><br /></li>
</ul>
</li>
<li><p><strong>Repository Compose: packages are transferred into the repository, according to a list</strong></p>
<ul>
<li>Compilation of a source package can produce multiple binary packages</li>
<li>Not all produced packages go in the final/official repositories</li>
<li>A &quot;compose&quot; file is a big list of which packages go in to the distro, and which do not</li>
<li>Rocky&#39;s compose file aims to 100% match RHEL, as per our charter :-)  <br /><br /></li>
</ul>
</li>
<li><p><strong>Repository metadata update and other tasks</strong></p>
<ul>
<li>The DNF repo with the new package is updated</li>
<li>Rocky changelog gets updated</li>
<li>Distributed mirror network picks up new package changes and clones<br /><br /></li>
</ul>
</li>
<li><strong>The package(s) are now ready to be installed!  <code>dnf</code> time!</strong></li>
</ol>
<p><br /></p>
<p>We&#39;ll explore each of these steps in a bit more detail below.  To illustrate the concepts all the way through, I&#39;ll use Nginx (a popular web server package) as an example we can refer to.</p>
<p>I intend to write a follow-up article shortly after this one which dives a bit more into the actual software pieces that perform these steps, and how they do it.  For now it&#39;s enough to know the basic tasks and why we&#39;re interested in doing them this way.</p>
<p><br /><br /></p>
<h2 id="step-1-source-import">Step 1: Source Import</h2>
<p>If we want to recompile RHEL, we just first obtain the sources for RHEL.  Fortunately for us, the sources are well-organized and always available.  They can be pulled from:  <a href="https://git.centos.org/">https://git.centos.org/</a> (spec files, patches), and <a href="https://git.centos.org/sources/">https://git.centos.org/sources/</a> (.tar source code archives).</p>
<p>In our Nginx example, the current version (as of this writing) is: <em>1.14.1-9</em>.  The RHEL source package is browseable  <a href="https://git.centos.org/rpms/nginx/tree/65d023199d2a81bb0e83e1147751c9fd2032b302">here</a>.  We can tell from the &quot;.nginx.metadata&quot; file in that repo that the source tarball SHA hash is:<br><code>a9dc8c5b055a3f0021d09c112d27422f45dd439c</code> (for file nginx-1.14.1.tar.gz).  That tarball (named after its hash) is downloadable at: <a href="https://git.centos.org/sources/nginx/c8-stream-1.14/">https://git.centos.org/sources/nginx/c8-stream-1.14/</a>.  Rocky clones that repository and copies that source tar file into our own infrastructure.  </p>
<p>The matching Rocky source repo for nginx-1.14 is here: <a href="https://git.rockylinux.org/staging/rpms/nginx/-/tree/r8-stream-1.14">https://git.rockylinux.org/staging/rpms/nginx/-/tree/r8-stream-1.14</a> and you can download the same source tarball from Rocky Linux here:  <a href="https://rocky-linux-sources-staging.a1.rockylinux.org/a9dc8c5b055a3f0021d09c112d27422f45dd439c">https://rocky-linux-sources-staging.a1.rockylinux.org/a9dc8c5b055a3f0021d09c112d27422f45dd439c</a>.</p>
<p><br /><br /></p>
<h2 id="step-2-patching-debranding-sources">Step 2: Patching/Debranding Sources</h2>
<p>The Nginx package contains Red Hat logos and references in its sources, mostly for the default error and &quot;welcome&quot; HTML pages that are bundled with it.  These must be replaced with Rocky Linux branding, we are NOT allowed to redistribute any trademarked material!</p>
<p>For packages that need it, Rocky hosts a per-package &quot;patch/&quot; repository at: <a href="https://git.rockylinux.org/staging/patch/">https://git.rockylinux.org/staging/patch/</a>.  For example, the Nginx patch repository can be found at: <a href="https://git.rockylinux.org/staging/patch/nginx">https://git.rockylinux.org/staging/patch/nginx</a>.  Any package without a matching &quot;patch/&quot; repository gets imported 100% unaltered from its RHEL source.</p>
<p><br /></p>
<p><strong>About Automation:</strong> We obviously don&#39;t want to import these packages by hand, there are over 5000 of them!  Rocky has a piece of in-house software, called  <strong>srpmproc</strong> ( <a href="https://github.com/rocky-linux/srpmproc">https://github.com/rocky-linux/srpmproc</a> ), which helps automate this process.  Srpmproc imports packages/sources, tags them appropriately, and applies Rocky patches if applicable.  </p>
<p><br /></p>
<p><strong>Final note about sources:</strong>  There is obviously much more to the source code / import process, this section has just skimmed the surface.  Source code management, import, and patching will get its own article in this series soon, look for it!</p>
<p><br /><br /></p>
<h2 id="step-3-compiling-sources">Step 3: Compiling Sources</h2>
<p>Once the source has been imported, we can actually perform a build!  RPMs are traditionally built with a program called <strong>rpmbuild</strong>.  Rocky builds with this, but there are more tools and abstractions built around it to help us compile packages at scale.</p>
<p><strong>The simple:  Mock</strong></p>
<p><a href="https://github.com/rpm-software-management/mock">Mock</a> is a build wrapper tool.  It will create a simple container/chroot environment and bootstrap a minimal system inside using DNF.  It will install only what is needed to build the package inside, and then calls rpmbuild within this &quot;clean&quot; environment.</p>
<p><strong>More complicated: Koji, MBS, Distrobuild</strong></p>
<p>These are tools built to call Mock (and rpmbuild) against many packages, and organize the resulting RPM files.</p>
<p><strong><a href="https://pagure.io/koji">Koji</a></strong> is the centralized build system of choice for Fedora-based distros.  </p>
<p><strong><a href="https://github.com/rocky-linux/distrobuild">Distrobuild</a></strong> is a web front-end tool built by the Rocky project to trigger Koji for builds, as well as call srpmproc to import RHEL sources.</p>
<p><strong><a href="https://pagure.io/fm-orchestrator">MBS</a></strong> Is the Module Build Service, which is responsible for triggering Koji for modular stream builds.  Modules and modular streams will get their very own article in this series later!</p>
<p><br /></p>
<p>There&#39;s obviously much more depth to these tools than this short introduction, and they&#39;ll be covered in more detail in subsequent posts.</p>
<p><br /></p>
<p><strong>Live, Browseable Rocky Tools:</strong></p>
<ul>
<li>Rocky Koji: <a href="https://kojidev.rockylinux.org/koji/">https://kojidev.rockylinux.org/koji/</a></li>
<li>Rocky Distrobuild: <a href="https://distrobuildstg.rockylinux.org/">https://distrobuildstg.rockylinux.org/</a></li>
<li>Distrobuild Nginx Imports+Builds:  <a href="https://distrobuildstg.rockylinux.org/packages/1950">https://distrobuildstg.rockylinux.org/packages/1950</a></li>
</ul>
<p><br /></p>
<p><img src="blog_image/rocky_build_infra.png" alt="Rocky build infrastructure"  title="Rocky build infrastructure"></p>
<p><strong>(Note: this png file has its diagram data embedded, and can be imported into <a href="https://app.diagrams.net/">https://app.diagrams.net/</a> for editing)</strong></p>
<p><br /></p>
<p>The build process flow of abstraction happens like this:</p>
<p><strong>Rocky Release Engineers</strong>  ---interacts with---&gt;  <strong>Distrobuild</strong>  ---calls API---&gt;  <strong>Koji</strong> ---triggers---&gt;  <strong>Mock</strong>  ---triggers---&gt;  <strong>rpmbuild</strong>  ---triggers---&gt;  <strong>gcc/g++/automake/Cmake/Maven(Java)/etc</strong></p>
<p><br /><br /></p>
<h2 id="step-4-sign-packages">Step 4: Sign packages</h2>
<p>Once the packages have been compiled, they sit in several large DNF repositories held and organized by Koji.  These packages are copied, submitted to Rocky&#39;s <a href="https://pagure.io/sigul">Sigul</a> instance, which then signs it with the Rocky Linux private key.  The freshly signed package is put in a special staging repository within Koji.  Once signed, it should be ready for release.</p>
<p>The matching public signing key is distributed in every Rocky installation (you can find it on yours:  <code>/etc/pki/rpm-gpg/RPM-GPG-KEY-rockyofficial</code>).  This makes it absolutely certain that the updates and packages you download from the Rocky repositories were in fact produced by the Rocky Linux project, and not forged or tampered with in any way!  Your DNF package manager will refuse to proceed if it downloads an unsigned or mis-signed package from the official Rocky repositories.</p>
<p><br /><br /></p>
<h2 id="step-5-repository-compose">Step 5: Repository Compose</h2>
<p>Now that we have packages signed and ready, it&#39;s time to get them into the official Rocky repositories, and into the hands of users (and mirrors) around the world!</p>
<p>Publishing is done with a Fedora tool called <a href="https://pagure.io/pungi">Pungi</a>.  Pungi is configured to read a JSON  &quot;compose&quot; file.  The compose file is effectively a huge list of packages for Pungi to pluck out of Koji and place in the official Rocky repositories.  It handles other compose tasks as well, such as generating the Rocky Linux ISO images.</p>
<p>The Rocky compose files are designed to match RHEL&#39;s exactly: we want the exact same packages without adding or removing <em>anything</em>.  Maximum compatibility with RHEL is the #1 technical goal of the project (as per the Rocky mission statement and charter).</p>
<p><strong>Example: Rocky Linux 8.4 compose:</strong>   <a href="https://git.rockylinux.org/rocky/pungi-rocky/-/blob/r8/rocky-packages.json">https://git.rockylinux.org/rocky/pungi-rocky/-/blob/r8/rocky-packages.json</a></p>
<p><br /><br /></p>
<h2 id="step-6-misc-publish-tasks">Step 6: Misc. Publish Tasks</h2>
<p>There are a few final tasks to perform when a new/updated pacakge is pushed into the official Rocky Linux repository.  These are:</p>
<ul>
<li>Pungi uses DNF libraries to produce and push official updated repodata</li>
<li>Rocky Linux release notes/changelog is updated : <a href="https://docs.rockylinux.org/release_notes/8-changelog/">https://docs.rockylinux.org/release_notes/8-changelog/</a> </li>
<li>Mirrors receive new packages and metadata as the sync with the main Rocky download site</li>
</ul>
<p><br /></p>
<h2 id="next-time-">Next Time!</h2>
<p>I&#39;m hoping to release a quick follow-up to this article, going through all the software tools (mock, koji, mbs, etc.) in a bit more depth.  </p>
<p><br /></p>
<p><strong>Contact</strong></p>
<p>Thanks for reading!  If you have any questions about the material, or I guess questions in general, feel free to ping me:</p>
<ul>
<li>Chat:  @skip77 on <a href="https://chat.rockylinux.org">https://chat.rockylinux.org</a></li>
<li>Email: skip (at) rockylinux.org</li>
</ul>
<p><br /></p>

