# CIQ and Rocky Linux

### CIQ != RockyLinux , a guide to avoid confusion

This is a first for me, a non-technical blog entry!  I see a lot of misconception out there about Rocky Linux and its sponsors, particularly Ctrl IQ, inc. ("CIQ").  I feel qualified to write some things up here, as I'm a member of both organizations. (but see disclaimer below!)  Hoping I can give an informed perspective on how all of this works.
  
<br />

## Disclaimers
I joined Rocky Linux as a volunteer, right when the project was founded in December 2020 (Release Engineering squad).  It's a great project, and I'm proud to be a volunteer even today.  One pandemic later, in 2022, I was looking to change jobs.  I was hired on by CIQ in mid-2022 as an engineer, in addition to continuing volunteer efforts with Rocky Linux.

I'm currently employed full-time at CIQ, and it's how I make my livelihood.  I believe this gives me great insight into these 2 orgs.  But I acknowledge that like everyone, I have bias.  Please keep that in mind when reading this article.  Or reading anything else for that matter!
  
As per usual, anything I write here is from my perspective alone.  I can only speak for myself and not for the Rocky Linux project, Rocky Enterprise Software Foundation (RESF), CIQ inc., or any other organization.


<br />

## Executive Summary (TLDR)

If you're pressed for time, here are the main points I'm trying to drive home with this post:

- Rocky Linux (a free Linux distro) and CIQ inc. (a small-ish for-profit company) are NOT the same thing.  Too many people erroneously conflate the 2.
- CIQ *is* an enthusiastic supporter of Rocky Linux, as it provides great value to the company. (and many other companies + individuals, of course)
- CIQ supports Rocky directly by contributing some of its employees time.  Other sponsors provide support for the project as well, both directly and indirectly.
- CIQ provides services, custom images, HPC support, and other cool things based on Rocky Linux.  And anyone else can too!  That's the beauty of a solid, common, and Free platform: anyone can use it in any way they please!
- The RESF (Rocky Enterprise Software Foundation) charter and by-laws prevent any single organization from gaining majority membership on the project's board of directors.  This restriction applies to all companies, including CIQ.

<br />

## Quick Introduction

Before we begin, it's important to define what we're talking about:

- **Rocky Linux**
<br />
Rocky Linux, in case you didn't know, is an open source project intended to be a rebuild ("clone") of Red Hat Enterprise Linux.  Legally this project is housed under the Rocky Enterprise Software Foundation (RESF), a public benefit corporation.  The project has many sponsors and contributors, both large and small.  It can be thought of as the spiritual successor to the original CentOS project, and largely shares the same goal.

- **CIQ**
<br />
CIQ ("Ctrl IQ, Inc.") is a small, for-profit (we hope!) company founded several years ago, pre-dating the formation of Rocky Linux.  The company has a varied portfolio, but deals primarily in software development, support, and consulting.  Particularly in the areas of high performance computing (HPC) and specialized enterprise Linux workloads.

<br />
It's important to keep these definitions in mind.  These 2 are NOT THE SAME, even if they are sometimes related. (as we'll see)

<br />  

## Early Days

CIQ and many of its customers in the HPC space were heavy users of CentOS and RHEL-based environments.  They like many orgs, were taken by surprise when Red Hat announced the former's cancellation at the end of 2020.  Something had to be done, but it wasn't clear exactly what.  There was discussion at the time of CIQ attempting some kind of continuation or fork of CentOS 8, perhaps with partner support.

On a whim, CIQ founder Greg Kurtzer invited the public at large to an HPC enthusiast chat area to talk about it.  The response from the community was overwhelming:  there was motivation, talent, and opportunity to form a robust successor to CentOS.  Rocky Linux was born out of this.

<br />

## Sponsorship

Even as a same-sources rebuild, building and maintaining a Linux distribution is a major undertaking.  A CentOS "reboot" is something many organizations find great value in.  Sponsors and supporters of the new Rocky Linux effort began to line up.

Naturally, CIQ was (and is) an enthusiastic sponsor of the project.  Sponsors and supporters contribute in many different ways.  Some offer web hosting, some provide bandwidth.  Others provide time from their employees:  engineers, testers, technical writers, web designers, etc.  Some contribute monetarily, but many supporters are simply active in the community:  Patrolling for bugs, helping users solve issues, and more.

As a smaller company, CIQ's main asset is its collection of engineering talent. (hi!)  This is the primary way the company can contribute to Rocky Linux:  some working time of some employees is set aside to help out on the project.  Building packages, ensuring robust infrastructure, writing documentation, and all those other helpful tasks that make a high quality Linux distribution.  But the load of creating and maintaining Rocky is shared.  Other companies contribute (some) employee time.  Most RESF/Rocky members are individual contributors whose employer does not pay them for their volunteer effort.  These contributors make up the bulk of the distro's teams:  testing, documentation, infrastructure, engineering, or Special Interest Groups (SIGs).

<br />

## Relationship

I hope I've painted a clear picture so far.  Some CIQ employees are *members* of the Rocky Linux project, including CEO Greg Kurtzer.  (and yours truly too!)  And CIQ acts as supporter and enthusiastic cheerleader for the project - like CentOS, it works great as a platform to customize and build things for HPC and enterprise customer workloads.  But CIQ is not "special" in this regard: The Rocky distribution is supported by a wide variety of interested people and organizations, same as many open source projects.

Used as a home server or supercompute platform, a freely available and redistributable build of Enterprise Linux is great for all kinds of different people.

<br />

## Influence and Project Direction

One major community concern at the outset of Rocky's founding was control.  Original CentOS was unilaterally canceled and taken in a wildly different direction, due to the project being completely controlled and funded by a single entity.  The consensus opinion was that should not be possible in Rocky.

Rocky Linux was built with an independent mindset, and this is reflected in the way its parent org, RESF, is set up.  Board members, governing laws, and meeting minutes are all available to the public here:  [https://www.resf.org/about](https://www.resf.org/about) .  

<br />

## Conclusion

My primary goal in writing this is to clear up misconceptions and give accurate information about how these orgs work.  I reiterate this is 100% my (Skip's) perspective, as I understand things.  I think I've accurately represented things,  at least as I see them.  As usual, I love to talk about this stuff - feel free to hit me up on Rocky Linux chat (chat.rockylinux.org), or the bridged Libera chat IRC:  I'm "skip77" on #rockylinux, #centos, and #centos-stream channels.  

<br />