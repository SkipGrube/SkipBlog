<h1 id="rocky-build-lab">Rocky Build Lab</h1>
<p><img src="blog_image/muppet_labs.jpg" alt="The Rocky Linux Dev Lab" title="The Rocky Linux Dev Lab"></p>
<p><em>Date: 2021-10-28</em></p>
<h2 id="intro">Intro</h2>
<p>It&#39;s lab time!  We can take some of what we learned in the previous articles, and put it into practice.  This is a practical guide to help you set up Mock, download a package&#39;s source, and build it!</p>
<p><br /></p>
<p><strong>Lightning Quick Review:</strong></p>
<p>Remember that <strong>mock</strong> is the program we use to build RPMs.  It sets up an isolated chroot environment, installs a minimal Rocky system inside, and installs only packages that are necessary for the build.</p>
<p>Also recall that our package sources are hosted at: <a href="https://git.rockylinux.org/staging/rpms">https://git.rockylinux.org/staging/rpms</a> , so we will definitely need <strong>git</strong> on our system to pull sources.</p>
<p>And if you remember from the sources article, archives for the source packages are located in Rocky&#39;s S3 storage: <strong><a href="https://rocky-linux-sources-staging.a1.rockylinux.org/{SHA1SUM}">https://rocky-linux-sources-staging.a1.rockylinux.org/{SHA1SUM}</a></strong> , so we&#39;ll want the <strong>curl</strong> utility to download from there.  (Or we can use wget, web browser, etc.)</p>
<p><br /></p>
<h2 id="install-setup">Install + Setup</h2>
<p>Mock is packaged for most RPM-based distributions, and even works on Debian/Ubuntu(!) due to its chroot/container workflow.  It doesn&#39;t matter too much what system you run it on, but I (of course) recommend Rocky 8, or some other RHEL 8 compatible system to try this.  That way you&#39;re guaranteed to be compatible with the instructions I&#39;m using here.</p>
<h3 id="do-i-need-any-special-hardware-">Do I need any special hardware?</h3>
<p>No!  Most anything will do.  I myself will be running these instructions on a Raspberry Pi 4 running the latest Rocky Linux 8.  Any laptop, desktop, or VM that runs Rocky should be able to manage this.  More memory and faster storage/processor will obviously speed your builds up though :-) .</p>
<p><br /></p>
<h3 id="part-1-install-software">Part 1: Install Software</h3>
<p>This is pretty easy, everything we want is already packaged for us.  Mock is in the <a href="https://docs.fedoraproject.org/en-US/epel/epel-about-history-philosophy/">EPEL</a> repository, so we&#39;ll need to enable that (don&#39;t worry, all it takes is a package install!)</p>
<p>First, let&#39;s install git and curl (or make sure they&#39;re already installed):</p>
<pre><code>dnf <span class="hljs-keyword">install</span> git curl
</code></pre><p><br /></p>
<p>Next, we can enable the EPEL repository and install mock:</p>
<pre><code>dnf <span class="hljs-keyword">install</span> epel-<span class="hljs-keyword">release</span>
dnf <span class="hljs-keyword">install</span> mock
</code></pre><p><br /></p>
<h3 id="part-2-users-groups-folders">Part 2: Users, Groups, Folders</h3>
<p>You can run <code>mock</code> builds as root and be fine, but generally we like to use an unprivileged user.  To do that, simply add your user to the &quot;mock&quot; group (that was created when you installed the mock package):</p>
<p><code>sudo usermod -G mock -a MYUSER</code></p>
<p>(Where MYUSER is the name of your user.  For example, mine is &quot;skip&quot;)</p>
<p><strong>Be sure to start up a new shell after this!</strong>  Your group membership doesn&#39;t take effect until you launch a new shell.</p>
<p>Check your group membership with:  <code>id</code> , make sure <strong>mock</strong> is listed in your groups list.</p>
<p><br /></p>
<p>Let&#39;s create some directories in our home folder to organize things: a place for code, and a place to build RPMs:</p>
<p><code>mkdir -p   ~/rockysrc  ~/rockybuild</code></p>
<p>Now our permissions are set, and we&#39;ve got some build space organized!</p>
<p><br /></p>
<h3 id="part-3-mock-config">Part 3: Mock Config</h3>
<p>Let&#39;s set up mock to build against Rocky Linux!</p>
<p>Take a look at the files under <strong>/etc/mock/</strong>: We see that there are tons of options, you can build packages for all KINDS of systems!</p>
<p>Notice that there is a <strong>default.cfg</strong> file, and it is a symbolic link to &quot;epel-8.cfg&quot;.  This default is for CentOS 8, building with EPEL repositories.  But we want to change it to Rocky!  Note that there&#39;s a <strong>rocky-8-x86_64.cfg</strong> file in there, let&#39;s point the default.cfg to that instead:</p>
<pre><code><span class="hljs-title">cd</span> /etc/mock
<span class="hljs-title">sudo</span> rm <span class="hljs-keyword">default</span>.cfg
<span class="hljs-title">sudo</span> ln -s rocky<span class="hljs-number">-8</span>-x86_64.cfg <span class="hljs-keyword">default</span>.cfg
</code></pre><p><strong>Side note:</strong> This assumes x86_64 (Intel) processor.  If you are using an ARM 64-bit processor (like a Raspberry Pi), please use the <strong>rocky-8-aarch64.cfg</strong>  config file instead.  Your builds much match your processor!</p>
<p><br />
Now we have mock ready to build our packages in a Rocky Linux chroot/container environment!  Let&#39;s build!</p>
<p><br />
<br /></p>
<h2 id="first-package-bash-">First package: Bash!</h2>
<p>First package is a simple one: everyone&#39;s favorite shell!  Ok, maybe not <em>everyone&#39;s</em>, but it&#39;s pretty darn popular.  Bash is a straightforward package, so let&#39;s build it!</p>
<h3 id="obtain-bash-sources">Obtain Bash Sources</h3>
<p>We know from the previous article on source control that Rocky&#39;s package sources are kept under <a href="https://git.rockylinux.org/staging/rpms/">https://git.rockylinux.org/staging/rpms/PACKAGENAME</a>.  So let&#39;s navigate to our rockysrc folder and clone bash.  We can then cd into the folder and make sure we have the proper &quot;r8&quot; (Rocky 8) branch checked out:</p>
<pre><code><span class="hljs-built_in">cd</span> ~/rockysrc
git <span class="hljs-built_in">clone</span> https://git.rockylinux.org/staging/rpms/bash

<span class="hljs-built_in">cd</span> bash
git branch
</code></pre><p>We have the bash spec file and patches, but we are missing the .tar.gz file - the actual bash source code!  Again referring back to my article on source control, we know that the file name and hash are listed under a <strong>.bash.metadata</strong> file in the repository.  Let&#39;s take a look:</p>
<pre><code><span class="hljs-comment"># (assuming you are in the bash/ folder we cloned):</span>

[skip<span class="hljs-variable">@RpiRockyDev</span> bash]<span class="hljs-variable">$ </span>cat .bash.metadata 
<span class="hljs-number">8</span>de012df1e4f3e91f571c3eb8ec45b43d7c747eb SOURCES/bash-<span class="hljs-number">4.4</span>.tar.gz
</code></pre><p>We see the SHA hash of the file, and the name/location where it belongs.  If you read the source article closely, you know that Rocky stores its sources on a web server with files labelled by SHA1 hash:  <a href="https://rocky-linux-sources-staging.a1.rockylinux.org/{SHA1SUM}">https://rocky-linux-sources-staging.a1.rockylinux.org/{SHA1SUM}</a>.  So let&#39;s download this file into SOURCES/ and name it appropriately:</p>
<pre><code><span class="hljs-comment"># (again, from inside the bash/ folder we cloned)</span>

curl   -o  SOURCES<span class="hljs-regexp">/bash-4.4.tar.gz   https:/</span><span class="hljs-regexp">/rocky-linux-sources-staging.a1.rockylinux.org/</span><span class="hljs-number">8</span>de012df1e4f3e91f571c3eb8ec45b43d7c747eb

ls -larth SOURCES<span class="hljs-regexp">/</span>
</code></pre><p>Now we have the RPM spec file and patches (from Git), and the source tarball (from the sources webserver).  We&#39;re ready to build!</p>
<p><br /></p>
<h3 id="make-an-srpm-using-mock">Make an SRPM using Mock</h3>
<p>In order to compile our Bash package, we need to first build the Source RPM (SRPM).  Fortunately, this is pretty straightforward.</p>
<p>We are going to keep all of our build output (logs and RPM files) organized in the <strong>~/rockybuild</strong> folder we made earlier.  You can organize things however you like, the way I&#39;m doing it here is just a suggestion.</p>
<pre><code># create <span class="hljs-keyword">bash </span><span class="hljs-keyword">build </span>output folder:
<span class="hljs-symbol">mkdir</span> -p ~/rockybuild/<span class="hljs-keyword">bash
</span>
# Compile SRPM from our spec file <span class="hljs-keyword">and </span>sources folder, <span class="hljs-keyword">and </span>put <span class="hljs-keyword">it </span>in the rockybuild/<span class="hljs-keyword">bash/ </span>folder:
<span class="hljs-symbol">mock</span> -v --resultdir=~/rockybuild/<span class="hljs-keyword">bash </span>  --<span class="hljs-keyword">buildsrpm </span>--spec=~/rockysrc/<span class="hljs-keyword">bash/SPECS/bash.spec </span> --sources=~/rockysrc/<span class="hljs-keyword">bash/SOURCES/</span>
</code></pre><p>There&#39;s a bit to unpack with that <strong>mock</strong> command.  We&#39;re telling it to:</p>
<ul>
<li>Be verbose (-v):</li>
<li>We want the results (log files and RPM products) in a certain place:  --resultdir= </li>
<li>We want to build a Source RPM from our checked out source code:  --buildsrpm </li>
<li>The Spec file we want to build the SRPM from:  --spec= </li>
<li>The SOURCES/ we want to build the SRPM from:  --sources=</li>
</ul>
<p>When the dust clears, we should now have a source RPM file in the <strong>~/rockybuild/bash/</strong> folder, named by version.  Mine is called:  <strong>bash-4.4.19-14.el8.src.rpm</strong> , but your version may be different as new versions of bash are released.</p>
<p><strong>Why use mock to create a simple SRPM?</strong>  We <em>could</em> have just used rpmbuild to produce the source RPM without the overhead of Mock.  In theory, there shouldn&#39;t be much difference.  However, the advantage of doing it in Mock is the Rocky mock config that we are using guarantees all of our RPM macros/variables are defined correctly.  Things like release tags (el8, el8_4, etc.) ought to be accurate.  It usually doesn&#39;t matter when doing private builds, but it can make a difference!</p>
<p><br /></p>
<h3 id="compile-bash">Compile Bash</h3>
<p>Finally!  Time to compile!  This is simple, just point Mock at your package and let er&#39; rip!  Like this:</p>
<pre><code><span class="hljs-comment"># (I like to use the "time" command to time my RPM compilations. You can completely remove it if you like)</span>

<span class="hljs-built_in">time</span>  mock  -v  <span class="hljs-comment">--resultdir=~/rockybuild/bash   ~/rockybuild/bash/bash-4.4.19-14.el8.src.rpm</span>
</code></pre><p>This should take a few minutes, depending on how fast your system is.  When the command finishes, you should now have RPMs and log files in your <strong>--resultdir</strong>!  Here is mine for reference:</p>
<pre><code>[skip@RpiRockyDev bash]$ ls -<span class="hljs-number">1</span> ~/rockybuild/bash/
bash-<span class="hljs-number">4.4</span>.<span class="hljs-number">19</span>-<span class="hljs-number">14</span><span class="hljs-selector-class">.el8</span><span class="hljs-selector-class">.aarch64</span><span class="hljs-selector-class">.rpm</span>
bash-<span class="hljs-number">4.4</span>.<span class="hljs-number">19</span>-<span class="hljs-number">14</span><span class="hljs-selector-class">.el8</span><span class="hljs-selector-class">.src</span><span class="hljs-selector-class">.rpm</span>
bash-debuginfo-<span class="hljs-number">4.4</span>.<span class="hljs-number">19</span>-<span class="hljs-number">14</span><span class="hljs-selector-class">.el8</span><span class="hljs-selector-class">.aarch64</span><span class="hljs-selector-class">.rpm</span>
bash-debugsource-<span class="hljs-number">4.4</span>.<span class="hljs-number">19</span>-<span class="hljs-number">14</span><span class="hljs-selector-class">.el8</span><span class="hljs-selector-class">.aarch64</span><span class="hljs-selector-class">.rpm</span>
bash-devel-<span class="hljs-number">4.4</span>.<span class="hljs-number">19</span>-<span class="hljs-number">14</span><span class="hljs-selector-class">.el8</span><span class="hljs-selector-class">.aarch64</span><span class="hljs-selector-class">.rpm</span>
bash-doc-<span class="hljs-number">4.4</span>.<span class="hljs-number">19</span>-<span class="hljs-number">14</span><span class="hljs-selector-class">.el8</span><span class="hljs-selector-class">.aarch64</span><span class="hljs-selector-class">.rpm</span>
build<span class="hljs-selector-class">.log</span>
hw_info<span class="hljs-selector-class">.log</span>
installed_pkgs<span class="hljs-selector-class">.log</span>
root<span class="hljs-selector-class">.log</span>
state.log
</code></pre><p><br /></p>
<p>If you have these files, then congrats!  You&#39;ve built a Rocky RPM, and you&#39;ve done it, at its core, <em>the same way our release team does</em>.  Only 3199 more to go.... ;-) .</p>
<p><br /></p>
<h2 id="recap-the-steps-we-took">Recap:  The Steps We Took</h2>
<p>Here is a quick review of the steps we just took to download the Bash source and compile it:</p>
<pre><code><span class="hljs-comment"># 1: Clone from Git:</span>
git clone <span class="hljs-symbol">https:</span>/<span class="hljs-regexp">/git.rockylinux.org/staging</span><span class="hljs-regexp">/rpms/bash</span>

<span class="hljs-comment"># 2: Retrieve the source tar and name it correctly (from the .bash.metadata file in the Git repository):</span>
curl   -o  SOURCES/bash-<span class="hljs-number">4.4</span>.tar.gz   <span class="hljs-symbol">https:</span>/<span class="hljs-regexp">/rocky-linux-sources-staging.a1.rockylinux.org/</span><span class="hljs-number">8</span>de012df1e4f3e91f571c3eb8ec45b43d7c747eb

<span class="hljs-comment"># 3: Build an SRPM from our sources:</span>
mock -v --resultdir=~<span class="hljs-regexp">/rockybuild/bash</span>   --buildsrpm --spec=~<span class="hljs-regexp">/rockysrc/bash</span><span class="hljs-regexp">/SPECS/bash</span>.spec  --sources=~<span class="hljs-regexp">/rockysrc/bash</span><span class="hljs-regexp">/SOURCES/</span>

<span class="hljs-comment"># 4: Build the package from the SRPM:</span>
time  mock  -v  --resultdir=~<span class="hljs-regexp">/rockybuild/bash</span>   ~<span class="hljs-regexp">/rockybuild/bash</span><span class="hljs-regexp">/bash-4.4.19-14.el8.src.rpm</span>
</code></pre><p>For basic packages, that&#39;s all there is to it!  Clone from git, grab the tarball, build SRPM, build binary package.  4 relatively straightforward steps.</p>
<p><br /></p>
<h2 id="the-lab-part-2-">The Lab: Part 2!</h2>
<p>This lab entry got a bit long, so I&#39;m splitting it into 2 parts.</p>
<p>I&#39;m releasing part 2 soon, where we&#39;ll compile <strong>bind</strong>, the famous DNS server!</p>
<p>It&#39;s a little tricky because there are some dependencies we&#39;ll need to dig through, unfortunately it doesn&#39;t <em>just work</em> like bash did.  </p>
<p><br /></p>
<h2 id="thanks-">Thanks!</h2>
<p>Thank you for reading!  Please feel free to ask questions in <a href="https://chat.rockylinux.org">chat</a> , or on <a href="https://www.reddit.com/r/RockyLinux/">reddit</a> if you want to talk about building packages.</p>
<p>See you in the next article, build lab #2!</p>
<p><br /></p>
<p>-Skip </p>
<p><br /></p>

