<h1 id="rocky-linux-series-2-meet-the-software-players">Rocky Linux Series #2:  Meet the (Software) Players</h1>
<p><img src="blog_image/rocky_build_system.png" alt="The Rocky Linux build system" title="The Rocky Linux build system"></p>
<p><em>Date: 2021-09-20</em></p>
<p>This post is a followup to the previous &quot;Build Steps&quot; entry.  Here I want to list out all of the pieces used to build the distro, say a few words about them, and provide links to see them in action.</p>
<p><br /></p>
<h2 id="the-list">The List</h2>
<p>Very brief, here is what we&#39;ll cover.  This list is roughly in order from &quot;small&quot; tools to &quot;larger&quot; ones.  Larger tools are abstractions on top of, and call the smaller tools.  For example, Mock calls rpmbuild, so Mock is a &quot;larger&quot; tool.  Mock is in turn called by Koji, so Koji is bigger than Mock.  I hope that makes sense, it&#39;s the best sorting system I could think of!</p>
<ul>
<li>rpmbuild</li>
<li>Mock</li>
<li>srpmproc</li>
<li>Koji</li>
<li>Module Build Service (MBS)</li>
<li>Sigul</li>
<li>Pungi</li>
<li>Distrobuild</li>
<li>Queue Systems: RabbitMQ / Celery / Fedmsg</li>
</ul>
<p><br /></p>
<h2 id="rpmbuild">rpmbuild</h2>
<p>Website : <strong><a href="https://rpm.org">https://rpm.org</a></strong></p>
<p>Source code : <strong><a href="https://github.com/rpm-software-management/rpm">https://github.com/rpm-software-management/rpm</a></strong></p>
<p>Written in : <strong>C</strong></p>
<p>Usually called by : <strong>Mock</strong></p>
<p><strong>rpmbuild</strong> is a classic tool that&#39;s still in use today.  Its job is simple:  take a source RPM containing the &quot;recipe&quot; for building it, and compile it into a binary RPM.  Source RPMs generally contain code, patches, and a .spec file describing version/dependencies/etc.</p>
<p>One of the weaknesses of traditional rpmbuild is the idea of &quot;contaminated dependencies&quot;.  Basically, the machine running rpmbuild is expected to have all the required RPMs already installed for the build.  For example, if a source RPM requires <code>automake</code> and <code>g++</code> to build, then those packages better be installed!  But a running server or desktop could have all kinds of extra packages installed that the code needs to compile, but may not be noticed in the RPM&#39;s .spec file.</p>
<p>Fortunately, we have a workaround:  a way to create a temporary container, with only the bare minimum of RPM dependencies installed.  We run rpmbuild inside the container, and can guarantee that we only have our dependent packages installed, and nothing else. This software is called <strong>Mock</strong>.</p>
<p><br /></p>
<h2 id="mock">Mock</h2>
<p>Website + Source Code : <strong><a href="https://github.com/rpm-software-management/mock">https://github.com/rpm-software-management/mock</a></strong></p>
<p>Written in : <strong>Python</strong></p>
<p>Usually called by : <strong>Koji</strong> (but can be run manually for local/test builds)</p>
<p><strong>Mock</strong> is a tool for building RPM packages.  Essentially, it creates a <code>chroot</code> or systemd container, and installs a bare minimum bootstrap system inside that chroot.  It then reads a source RPM&#39;s build-time requirements, and performs a <code>dnf install</code> of those requirements inside the container.  </p>
<p>Now that the container has the smallest possible amount of requirements installed, Mock performs an <code>rpmbuild</code> against the source RPM inside the container, just as if it was being done on a regular system.  Doing it this way we guarantee that only the necessary packages are installed, and we don&#39;t have to constantly install/uninstall dependencies on a build server to satisfy rpmbuild.  We can do it all inside the minimal container, which then gets thrown away after we&#39;re done!</p>
<p>Mock is incredibly flexible via its config file, allowing the minimal bootstrap environment to be customized, pre- and post- actions to be run inside the container during build, and all kinds of things RPM-builders would be interested in.  Check out its documentation for further info.</p>
<p><br /></p>
<h2 id="srpmproc">Srpmproc</h2>
<p>Source code : <strong><a href="https://github.com/rocky-linux/srpmproc">https://github.com/rocky-linux/srpmproc</a></strong></p>
<p>Written in : <strong>Go</strong></p>
<p>Usually called by : <strong>Distrobuild</strong></p>
<p><strong>Srpmproc</strong> is a tool developed by the Rocky Linux project, for our purposes.  It has many options, but its core purpose is simple:  Take source code for building RPMs from one repository, and put it in another.  If the code/RPM spec from the original repository requires any patching or transformation, it does that as well, according to a standardized language.  For the Rocky Linux project, <code>srpmproc</code> pulls sources from <a href="https://git.centos.org">git.centos.org</a>  and imports them into <a href="https://git.rockylinux.org">git.rockylinux.org</a></p>
<p>This is a little more complicated than it sounds, as the program has to contend with things like branch name translation, non-trivial patch operations during import, and the import of source tarballs not stored with the main git repository.  Srpmproc will feature more prominently in my next article, which covers source code import, patching, and debranding.</p>
<p><br /></p>
<h2 id="koji">Koji</h2>
<p>Source code : <strong><a href="https://pagure.io/koji/">https://pagure.io/koji/</a></strong></p>
<p>Home Page : <strong><a href="https://fedoraproject.org/wiki/Koji">https://fedoraproject.org/wiki/Koji</a></strong></p>
<p>Written in : <strong>Python</strong></p>
<p>Usually called by : <strong>Distrobuild, Human user in web browser or Koji API</strong></p>
<p>Rocky Live Instance : <strong><a href="https://koji.rockylinux.org/koji/">https://koji.rockylinux.org/koji/</a></strong></p>
<p><strong>Koji</strong> is a centralized build system for RPM packages.  It integrates several components, including a web interface, API, internal yum repositories, and distributed builder hosts to do the actual package building.  Other programs (like Distrobuild) generally query or control Koji via its robust API and Kerberos authentication system.</p>
<p>Koji <a href="https://koji.rockylinux.org/koji/hosts">&quot;Hosts&quot;</a> are where the actual compilation is done.  They simply check for instructions from the Kojihub instance, and execute Mock/rpmbuild with the appropriate options enabled to perform the package build.  We have build hosts for each processor architecture that Rocky supports.  Currently that means we have x86_64 and aarch64 (ARM 64-bit), and PPC64 (PowerPC) is likely on the way soon. (<em>fingers crossed!</em>)</p>
<p>Builds in Koji are organized according to <em>targets</em> and <em>tags</em>.  A package build is associated with a target, while the resulting package is associated with a tag.  This allows us to organize packages according to release, generally by minor version of the distro.  For example, Rocky 8.4 updates (the current minor release), builds are done against the dist-rocky8_4-updates target, and package artifacts are tagged with the dist-rocky8_4-updates-build tag when builds complete.  There may be a future article where we dive into Koji targets, tags, repositories, and how they work together.</p>
<p><br /></p>
<h2 id="module-build-service-mbs-">Module Build Service (MBS)</h2>
<p>Source code / home page : <strong><a href="https://pagure.io/fm-orchestrator">https://pagure.io/fm-orchestrator</a></strong></p>
<p>Written in : <strong>Python</strong></p>
<p>Usually called by : <strong>Distrobuild</strong></p>
<p>Intro to RPM Package Modules : <strong><a href="https://docs.fedoraproject.org/en-US/modularity/">https://docs.fedoraproject.org/en-US/modularity/</a></strong></p>
<p><strong>MBS</strong> is the Module Build Service, a way to build modular streams in Fedora and other RPM-based distros, including RHEL (and therefore Rocky!).</p>
<p><strong>Quick crash course:</strong>  <em>Modularity</em> is a way to get multiple versions of the same software available for install in an RPM-based distro.  For example, in RHEL+Rocky, the PHP language has 3 different versions available for installation:  7.2, 7.3, and 7.4, with the default being 7.2.  You can flip between versions by enabling the proper version using the <code>dnf module</code> subcommand.  Then, when you <code>dnf install php</code>, DNF will &quot;see&quot; only the enabled version and its related packages.  You can get a quick summary of all the module versions available to you with:  <code>dnf module list</code>.  Try it and see what&#39;s there!</p>
<p>Modules are a really cool feature, but they do place an extra complexity on development.  Collections of module packages must be compiled extremely carefully, because they will need different chains of dependencies based on which version is being built.  PHP 7.2 cannot have the same dependency chain as PHP 7.4!  They are simply too different.  We have to carefully control this at build-time, which is where the Module Build Service comes in.</p>
<p>MBS is called via its own API.  Its primary purpose is to talk to Koji, and submit module builds in groups with special settings according to that module&#39;s needs.  For example, certain modules might need special RPM build macros assigned, or depend on specific versions of other modules.  And all of that must be controlled to ensure the packages within the module are built correctly and have a dependency chain that will work properly for end users.</p>
<p>Modularity in general is a big topic, and will get its own article in this series.  It&#39;s important to know that MBS has its own API, and acts as a special client to Koji.  It is concerned with organizing and submitting modular package builds.</p>
<p><br /></p>
<h2 id="sigul">Sigul</h2>
<p>Source code / home page : <strong><a href="https://pagure.io/sigul">https://pagure.io/sigul</a></strong></p>
<p>Written in : <strong>Python</strong></p>
<p>Usually called by : <strong>Distrobuild</strong></p>
<p><strong>Sigul</strong> is the Fedora project&#39;s signing system for packages, and Rocky Linux uses it as well.  It has an API &quot;bridge&quot; that lives on a listener server, which accepts logins and signing requests.  The bridge server then talks on a private network to a private signing/master server, which has the private release keys that actually sign the packages.  As mentioned before, we sign packages with our private key so end-users can be mathematically certain that the packages they download actually came from the Rocky Linux project.</p>
<p>Sigul can be called manually, but generally it is part of the automatic package build process orchestrated by <strong>Distrobuild</strong>.  First, Koji (and sometimes MBS) are called to execute the actual source checkout, compilation, etc.  Then Distrobuild is able to grab the freshly created packages from Koji, automatically submit them to Sigul for signing, and Sigul produces a package cryptographic signature.  That signature is then imported into Koji, and Koji will combine the signature and package file to produce a signed package.</p>
<p>Obviously Sigul is quite powerful, but our use case is fairly simple.  Submit unsigned RPM as input, receive a signature back as output.   Then instruct Koji to combine signature and RPM, and put the resulting signed file into the appropriate place where the release process can continue.  High availability and capacity are important for our Sigul installation - we sign a LOT of RPMs!</p>
<p><br /></p>
<h2 id="pungi">Pungi</h2>
<p>Source code / home page : <strong><a href="https://pagure.io/pungi">https://pagure.io/pungi</a></strong></p>
<p>Written in : <strong>Python</strong></p>
<p>Rocky 8 Pungi Config : <strong><a href="https://git.rockylinux.org/rocky/pungi-rocky/-/tree/r8">https://git.rockylinux.org/rocky/pungi-rocky/-/tree/r8</a></strong></p>
<p>Usually called by : <strong>Rocky Linux Release Engineer</strong></p>
<p><strong>Pungi</strong> is a distribution &quot;compose&quot; tool.  It creates (composes) both our official repositories (BaseOS, AppStream, etc.), as well as the bootable Rocky Linux disk images (ISOs).  It is a non-trivial tool, and has several sub-modules for doing all of these different tasks.</p>
<p>Its primary purpose is to read a manifest of packages that belong in the official repositories.  It connects to Koji, plucks those select packages out of the build system, and puts them in a staging area where repositories are built.  Pungi also creates the Rocky ISO installation media according to a list of packages as well.  It specifies what gets included in each ISO image (Everything, Minimal, Boot).</p>
<p>When packages are produced, not all of them make it into the final repositories.  Debug packages, many -devel packages, and others are not included, and Pungi helps us filter what stays and what goes.  Ultimately, Rocky Linux strives to match the RHEL composes package-for-package, version-for-version.  This means that a lot of packages which we&#39;d like to include get built, but ultimately left out of the official distribution.  We try to make these packages available through special -devel repositories.  They are always downloadable from Koji (or a Koji mirror) as a last resort, we want to make as much software available as possible!</p>
<p>Rocky makes our Pungi configs available in Git, via the link above.</p>
<p><br /></p>
<h2 id="distrobuild">Distrobuild</h2>
<p>Source code : <strong><a href="https://github.com/rocky-linux/distrobuild">https://github.com/rocky-linux/distrobuild</a></strong></p>
<p>Written in : <strong>Python</strong></p>
<p>Usually called by : <strong>Rocky Linux Release Engineer</strong></p>
<p>Rocky Live Instance : <strong><a href="https://distrobuildstg.rockylinux.org/">https://distrobuildstg.rockylinux.org/</a></strong></p>
<p><strong>Distrobuild</strong> is a Rocky Linux-specific tool, used primarily as a single web UI that makes interacting with several of these disparate tools much easier.  From Distrobuild, Rocky devs can import (and debrand) source code, launch package builds (&quot;normal&quot; packages or modules!), sign packages, and view history of a package&#39;s builds/imports.</p>
<p>It serves web content in its own right, but also acts as a client to several of these components: Koji (builds), MBS (module builds), Sigul (package signing).  It also calls Srpmproc to import sources from RHEL and run automated debranding/patches en masse.</p>
<p>The idea was to have a single point which could automate much of the nuts-and-bolts of the build process, and look half-way decent while doing it.  Koji&#39;s web interface is a logical place to do this, but it&#39;s a bit clunky and just doesn&#39;t integrate well with other pieces of build infrastructure, particularly module builds.</p>
<p>In the spirit of openness and full disclosure, Rocky&#39;s Distrobuild instance (perhaps the only one in the world?) is available publicly for browsing.  You can see logs of all source imports, status of all package builds, and essentially view a history of every build the Rocky team has ever done up to this point.  Pretty neat!</p>
<h2 id="queue-systems">Queue Systems</h2>
<p>RabbitMQ : <strong><a href="https://www.rabbitmq.com/">https://www.rabbitmq.com/</a></strong></p>
<p>Celery : <strong><a href="https://docs.celeryproject.org/en/stable/">https://docs.celeryproject.org/en/stable/</a></strong></p>
<p>Fedmsg : <strong><a href="https://github.com/fedora-infra/fedmsg">https://github.com/fedora-infra/fedmsg</a></strong></p>
<p>These 3 are all queue/task systems used by other pieces of the build system.  Koji prefers the (dated) fedmsg (Fedora messaging system), while other pieces use a combination of the Celery/RabbitMQ systems.</p>
<p>The details of these are important, but can&#39;t be covered in-depth here.  The important thing is that different pieces of build infrastructure can reliably talk to each other via these queue systems. Messages can be added to a queue to assign tasks, and consumed from the queue to execute those tasks.</p>
<h2 id="next-time-">Next Time...</h2>
<p>The next topic I&#39;ll cover in this journey is source code management.  We will see in-depth how (and where!) the sources are stored, how things are organized, and discover why exactly it&#39;s done this way.  <em>There will be lots of Git.</em>  Should be fun!</p>
<p>Thanks for reading.</p>
<p>-Skip</p>

